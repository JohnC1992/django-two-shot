from django.urls import path
from accounts.views import user_login, user_signout, signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_signout, name="logout"),
    path("signup/", signup, name="signup"),
]
